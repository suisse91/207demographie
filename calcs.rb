#!/usr/bin/env ruby

def dSommeX (lX, lY)
  somme = 0.0
  tmp = lX.zip(lY)
  tmp.each { |elem|
    x = elem[0]
    y = elem[1]
    somme += (x * y) if x && y
    somme += x if x && !y
    somme += y if !x && y
  }
  somme
end

def sommeX (lst)
  res = 0.0
  lst.each { |elem|
    res += elem ** 2
  }
  res
end

def calcAB (lX, lY)
  n = lY.length
  a = ((lY.inject(:+) * sommeX(lX)) - (lX.inject(:+) * dSommeX(lX, lY))) / ((n * sommeX(lX)) - (lX.inject(:+) ** 2))
  b = ((n * dSommeX(lX, lY)) - (lX.inject(:+) * lY.inject(:+))) / ((n * sommeX(lX)) - lX.inject(:+) ** 2)
  [a, b]
end

def fx (val, a, b)
  ((b * val) + a)
end

def ecartTypeYX (lX, lY, a, b)
  res = 0.0
  tmp = lX.zip(lY)
  tmp.each { |elem|
    x = elem[0]
    y = elem[1]
    res += ((y - fx(x, a, b)) ** 2)
  }
  res /= lY.length
  Math.sqrt res
end

def ecartTypeXY (lY, a, b)
  res = 0.0
  tmp = $years.zip(lY)
  tmp.each { |elem|
    x = elem[0]
    y = elem[1]
    res += ((y - ((x - a) / b)) ** 2)
  }
  res /= lY.length
  Math.sqrt(res)
end           

def covXY (lX, lY)
  res = 0.0
  moyX = lX.inject(:+) / lX.length
  moyY = lY.inject(:+) / lY.length
  tmp = lX.zip(lY)
  tmp.each { |elem|
    x = elem[0]
    y = elem[1]
    res += (x - moyX) * (y - moyY)
  }
  res / lY.length
end

def var (lst)
  res = 0.0
  moyl = lst.inject(:+) / lst.length
  lst.each { |elem|
    res += ((elem - moyl) ** 2)
  }
  res / lst.length
end     

def coeffCorrelation (cL)
  return covXY($years, cL) / Math.sqrt(var($years) * var(cL))
end
