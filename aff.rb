#!/usr/bin/env ruby
# coding: utf-8

def usage
  puts "Usage: ./207demographie [Codes pays]"
  exit
end

def affCountries (cL)
  puts "pays: "
  cL.each { |country|
    puts "\t#{country[0]}"
  }
end

def standardOut (cL)
  ajustYX(cL)
  ajustXY(cL)
  puts "coefficient de corrélation : #{coeffCorrelation(cL).round(4)}"
end
