#!/usr/bin/env ruby

require 'gruff'

def trace (cL)
  g = Gruff::Line.new
  g.title = '207demographie'
  gYears = Array.new
  for i in 1960...2051
    gYears << i
  end
  hYears = Hash.new
  gYears.each_with_index { |v, idx|
    hYears[idx] = v.to_s if v % 10 == 0
  }
  g.labels = hYears
  g.maximum_value = 500
  g.minimum_value = 400
  g.y_axis_increment = 50
  g.hide_dots = true
  g.data 'moyenne demographique'.to_s, cL
  g.data 'ajustement de Y en X'.to_s, $graphYX
  g.data 'ajustement de X en Y'.to_s, $graphXY
  g.write('demo.png')
  
end
